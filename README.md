## Documentation lu-fa

### Back-End
- PHP 7.0

### Front-End Stack

- Gulp
- Webpack
- SASS
- CSS3
- HTML5
- Vanilla Javascript / EcmaScript 6+

### Running locally

All front-end dependencies are managed through NPM, so make sure that you have installed.

### tech stack:

- Nodejs > 6.0
- PHP 7.0

### steps
1. Firstly, you have to update the dependencies `npm install`
2. Then access your browser http://localhost
3. Finally, to watch changes on Sass files run `gulp` in another terminal.

### Front-End project structure

```
public/
├── css/
│   ├── pageName.css - compiled and minified css files
├── js/
│   ├── app.bundle.js - transpiled javascript files
├── images/
│   ├── all project images
src/
├── sass/
│   ├── components/ - all sass specific components
│   ├── layout/ - layout components
│   ├── pages/ - page styles
├── js/
│   ├── components/ - all js specific components
│   ├── app.js - main javascript that control all dependencies
│
```

### Tests and validation

#### Desktop Browsers:

- IE 11+
- Chrome
- Firefox
- Safari

#### Mobile devices screen resolution:

- Galaxy Note 3
- LG Optimus L70
- Nexus 5X
- Iphone 5
- Iphone 7
- Iphone 7 Plus
- iPad mini
- iPad
- iPad Pro

### Performance Budget

- Not defined


### Build

- All deploy will be managed by bitbucket-pipelines

### Live

https://lu-fa.herokuapp.com/