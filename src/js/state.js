let Profile = {
    label: '',
    name: '',
    website: '',
    phone: '',
    place: '',
    selected: '',
    value: '',
    lastname: ''
}

module.exports = {
    profile: Profile
}