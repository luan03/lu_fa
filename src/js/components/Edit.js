import { profile } from '../state'

class Edit {

  desktop() {

    const markup = `
      <div class="popup">
        <div class="form" id="form-desktop">
          <label class="label" for="field">
            <input type="text" id="field" maxlength="255">
            <span class="placeholder">${profile.label}</span>
          </label>
          <span class="button button--save" id="save">save</span>
          <span class="button button--cancel" id="cancel">cancel</span>
        </div>
      </div>
    `;

    return markup
  }

  mobile() {
    const markup = `
      <div class="popup">
          <h2>About</h2>
          <div class="options">
              <span class="button button--mobile-form" id="cancel">cancel</span>
              <span class="button button--mobile-form" id="save">save</span>
          </div>
          <div class="form" id="form-mobile">
              <label class="label" for="firstName">
                <input type="text" id="firstName" maxlength="255" data-state="name">
                <span class="placeholder">First name</span>
              </label>

              <label class="label" for="lastName">
                <input type="text" id="lastName" maxlength="255" data-state="lastname">
                <span class="placeholder">Last name</span>
              </label>

              <label class="label" for="website">
              <input type="text" id="website" maxlength="255" data-state="website">
              <span class="placeholder">website</span>
              </label>

              <label class="label" for="phone">
              <input type="text" id="phone" maxlength="255" data-state="phone">
              <span class="placeholder">phone number</span>
              </label>

              <label class="label" for="place">
              <input type="text" id="place" maxlength="255"  data-state="place">
              <span class="placeholder">city, state & zip</span>
              </label>
          </div>
      </div>
    `;

    return markup
  }

  template() {

    let resolution = document.getElementsByTagName('body')[0].clientWidth
    let minimum_resolution = 845

    let template = (resolution <= minimum_resolution) ? this.mobile() : this.desktop()

    document.querySelector('#popup').innerHTML = template
  }

  init() {
    let self = this; // I dont use ; as pattern in my code. However, In this case needs due to ...selector

    [...document.querySelectorAll('[data-edit]')].map(el =>
      el.addEventListener('click', function() {

        profile.label = this.dataset.edit
        profile.selected = this.dataset.target

        self.template()
        self.close()
        self.save()
      })
    )
  }

  close() {
    let self = this;

    document.querySelector('#cancel').addEventListener('click', () => {
      self.removeTemplate()
    })
  }

  removeTemplate() {
    document.querySelector('#popup').innerHTML = ''
  }

  save() {
    let self = this;

    document.querySelector('#save').addEventListener('click', function() {

      //Update desktop
      [...document.querySelectorAll('[data-update='+ profile.selected +']')].map(field => {

        //check if edit field has a value
        if (this.previousElementSibling.childNodes[1].value !== '') {
          field.childNodes[0].nodeValue = this.previousElementSibling.childNodes[1].value
        }
      });

      //TODO: set full name

      //Update mobile
      [...document.querySelectorAll('#form-mobile input')].map(input => {

          if (input.value !== '') {

            //update state
            profile[input.dataset.state] = input.value;

            //update view
            [...document.querySelectorAll('[data-update='+ input.dataset.state +']')].map(field => {
              field.childNodes[0].nodeValue = input.value
            });
          }
      });

      self.removeTemplate();
    })
  }

  load() {
    this.init()
  }
}

export const edit = new Edit()