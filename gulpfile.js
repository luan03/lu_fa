const gulp      =   require('gulp');
const sass      =   require('gulp-sass');
const minifyCSS =   require('gulp-csso');
const babel     =   require('gulp-babel');
const webpack   =   require('webpack-stream');

gulp.task('js', () =>
    gulp.src('./src/js/app.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(webpack({
            output: { // options related to how webpack emits results

                filename: 'app.bundle.js', // string
            }
        }))
        .pipe(gulp.dest('public/js'))
);

gulp.task('css', () =>
    gulp.src('./src/sass/pages/*.scss')
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('public/css'))
);

gulp.task('watch', function() {
    gulp.watch('./src/sass/**/*.scss', ['css']);
    gulp.watch('./src/js/**/*.js', ['js']);
});

gulp.task('default', ['watch']);