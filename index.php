<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lu Fa</title>

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,600" rel="stylesheet">
    <link rel="stylesheet" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="public/css/home.css">
</head>
<body>
    <header>
        <div class="profile">
            <span class="button button--log-out">log out</span>
            <span class="button button--upload ion-ios-camera">Upload cover image</span>
            <div class="info">
                <span class="info__picture"></span>
                <strong class="info__field info__field--name" data-update="name">Jessica Parker</strong>
                <span class="info__field info__field--place ion-ios-location-outline" data-update="place">Newport Beach, CA</span>
                <span class="info__field info__field--phone ion-ios-telephone-outline" data-update="phone">(949) 325 - 68594</span>

                <div class="rating">
                    <ul class="stars">
                        <li class="ion-android-star"></li>
                        <li class="ion-android-star"></li>
                        <li class="ion-android-star"></li>
                        <li class="ion-android-star"></li>
                        <li class="ion-android-star-outline"></li>
                    </ul>
                    <span class="reviews"><span class="number">6</span> Reviews</span>
                </div>
            </div>
        </div>
        <nav class="menu">
            <div class="navigation">
                <ul>
                    <li class="menu__link--active">ABOUT</li>
                    <li>SETTINGS</li>
                    <li>OPTION1</li>
                    <li>OPTION2</li>
                    <li>OPTION3</li>
                </ul>
            </div>
            <span class="followers">
                <span class="ion-ios-plus"></span>
                <b>15</b>
                followers
            </span>
        </nav>
    </header>
    <section class="content">
        <div class="wrapper">
            <h2>About</h2>

            <span class="edit ion-edit" data-edit></span>

            <div class="list">
                <div id="popup"></div>

                <ul>
                    <li data-update="name">
                        Jessica Parker
                        <span class="list__edit ion-edit" data-edit="name" data-target="name"></span>
                    </li>
                    <li class="ion-earth" data-update="website">
                        www.seller.com
                        <span class="list__edit ion-edit" data-edit="website" data-target="website"></span>
                    </li>
                    <li class="ion-ios-telephone-outline" data-update="phone">
                        (949) 325 - 68594
                        <span class="list__edit ion-edit" data-edit="phone number" data-target="phone"></span>
                    </li>
                    <li class="ion-ios-home-outline" data-update="place">
                        Newport Beach, CA
                        <span class="list__edit ion-edit" data-edit="city, zip & state" data-target="place"></span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <script src="public/js/app.bundle.js"></script>
</body>
</html>