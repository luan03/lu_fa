/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_edit__ = __webpack_require__(1);


__WEBPACK_IMPORTED_MODULE_0__components_edit__["a" /* edit */].load()

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__state__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__state___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__state__);


class Edit {

  desktop() {

    const markup = `
      <div class="popup">
        <div class="form" id="form-desktop">
          <label class="label" for="field">
            <input type="text" id="field" maxlength="255">
            <span class="placeholder">${__WEBPACK_IMPORTED_MODULE_0__state__["profile"].label}</span>
          </label>
          <span class="button button--save" id="save">save</span>
          <span class="button button--cancel" id="cancel">cancel</span>
        </div>
      </div>
    `;

    return markup
  }

  mobile() {
    const markup = `
      <div class="popup">
          <h2>About</h2>
          <div class="options">
              <span class="button button--mobile-form" id="cancel">cancel</span>
              <span class="button button--mobile-form" id="save">save</span>
          </div>
          <div class="form" id="form-mobile">
              <label class="label" for="firstName">
                <input type="text" id="firstName" maxlength="255" data-state="name">
                <span class="placeholder">First name</span>
              </label>

              <label class="label" for="lastName">
                <input type="text" id="lastName" maxlength="255" data-state="lastname">
                <span class="placeholder">Last name</span>
              </label>

              <label class="label" for="website">
              <input type="text" id="website" maxlength="255" data-state="website">
              <span class="placeholder">website</span>
              </label>

              <label class="label" for="phone">
              <input type="text" id="phone" maxlength="255" data-state="phone">
              <span class="placeholder">phone number</span>
              </label>

              <label class="label" for="place">
              <input type="text" id="place" maxlength="255"  data-state="place">
              <span class="placeholder">city, state & zip</span>
              </label>
          </div>
      </div>
    `;

    return markup
  }

  template() {

    let resolution = document.getElementsByTagName('body')[0].clientWidth
    let minimum_resolution = 845

    let template = (resolution <= minimum_resolution) ? this.mobile() : this.desktop()

    document.querySelector('#popup').innerHTML = template
  }

  init() {
    let self = this; // I dont use ; as pattern in my code. However, In this case needs due to ...selector

    [...document.querySelectorAll('[data-edit]')].map(el =>
      el.addEventListener('click', function() {

        __WEBPACK_IMPORTED_MODULE_0__state__["profile"].label = this.dataset.edit
        __WEBPACK_IMPORTED_MODULE_0__state__["profile"].selected = this.dataset.target

        self.template()
        self.close()
        self.save()
      })
    )
  }

  close() {
    let self = this;

    document.querySelector('#cancel').addEventListener('click', () => {
      self.removeTemplate()
    })
  }

  removeTemplate() {
    document.querySelector('#popup').innerHTML = ''
  }

  save() {
    let self = this;

    document.querySelector('#save').addEventListener('click', function() {

      //Update desktop
      [...document.querySelectorAll('[data-update='+ __WEBPACK_IMPORTED_MODULE_0__state__["profile"].selected +']')].map(field => {

        //check if edit field has a value
        if (this.previousElementSibling.childNodes[1].value !== '') {
          field.childNodes[0].nodeValue = this.previousElementSibling.childNodes[1].value
        }
      });

      //TODO: set full name

      //Update mobile
      [...document.querySelectorAll('#form-mobile input')].map(input => {

          if (input.value !== '') {

            //update state
            __WEBPACK_IMPORTED_MODULE_0__state__["profile"][input.dataset.state] = input.value;

            //update view
            [...document.querySelectorAll('[data-update='+ input.dataset.state +']')].map(field => {
              field.childNodes[0].nodeValue = input.value
            });
          }
      });

      self.removeTemplate();
    })
  }

  load() {
    this.init()
  }
}

const edit = new Edit()
/* harmony export (immutable) */ __webpack_exports__["a"] = edit;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

let Profile = {
    label: '',
    name: '',
    website: '',
    phone: '',
    place: '',
    selected: '',
    value: '',
    lastname: ''
}

module.exports = {
    profile: Profile
}

/***/ })
/******/ ]);